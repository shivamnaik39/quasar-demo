const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue"), name: "Home" },
      {
        path: "admindashboard/:username",
        name: "AdminDashboard",
        component: () => import("pages/AdminDashboard.vue"),
        props: true
      },
      {
        path: "userdashboard/:username",
        name: "UserDashboard",
        component: () => import("pages/UserDashboard.vue"),
        props: true
      },
      {
        path: "userlist",
        name: "UserList",
        component: () => import("pages/UserList.vue")
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;

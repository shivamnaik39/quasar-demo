export function addUser(state, userDeatails) {
  // console.log(userDeatails);

  const userName = userDeatails.username;
  const user = state.users.find(user => user.username === userName);

  if (user) {
    return alert(`${userName} already exists!`);
  }

  state.users.push(userDeatails);
  alert(`${userDeatails.username} was added successfully`);
}

export function getUsers(state) {
  return state.users.filter(user => user.designation === "User");
}

export function getAdmins(state) {
  return state.users.filter(user => user.designation === "Admin");
}

export function getUser(state, username) {
  return username =>
    state.users.find(user => {
      return user.username === username;
    });
}

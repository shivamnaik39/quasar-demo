export default function() {
  return {
    users: [
      {
        username: "steve35",
        fullName: "Steve Smith",
        emailID: "stevesmith@exaple.com",
        phoneNO: "7445632145",
        designation: "User",
        userID: "55555"
      },
      {
        username: "sangak88",
        fullName: "Kumar Sangakara",
        emailID: "ksangakara@exaple.com",
        phoneNO: "5887412698",
        designation: "User",
        userID: "54889"
      },
      {
        username: "brian400",
        fullName: "Brian Lara",
        emailID: "brianlara@exaple.com",
        phoneNO: "5785419846",
        designation: "User",
        userID: "45872"
      },
      {
        username: "ravi33",
        fullName: "Ravi Shastri",
        emailID: "shastriravi@exaple.com",
        phoneNO: "8974125635",
        designation: "Admin",
        userID: null
      },
      {
        username: "jonty93",
        fullName: "Jonty Rhodes",
        emailID: "joneyrhodes@exaple.com",
        phoneNO: "8741254934",
        designation: "Admin",
        userID: null
      }
    ]
  };
}
